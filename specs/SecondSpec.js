// Create First spec in Protractor

describe("Registration and Login E2E Scenario", function ()
    {

    it('Register new user to application', function () {
                browser.driver.manage().window().maximize();
                browser.findElement(by.partialLinkText('have an account?')).click();
                browser.sleep(4000);

                browser.findElement(by.name('firstname')).sendKeys('Hello');
                browser.findElement(by.name('lastname')).sendKeys('Testing');
                browser.findElement(by.name('email')).sendKeys('HelloTesting@gmail.com');
                browser.findElement(by.name('password')).sendKeys('123456');
                browser.findElement(by.name('confirm_password')).sendKeys('123456');
                browser.findElement(by.xpath('//label[text()="Agree with the Terms & Conditions."]')).click();
                browser.findElement(by.buttonText('Register')).click();
                browser.sleep(4000);
            });

    it('Login to Application', function () {
               browser.driver.sleep(4000);
               browser.findElement(by.model('login.email')).clear();
               browser.findElement(by.model('login.email')).sendKeys(browser.params.login.username);
               browser.findElement(by.model('login.password')).sendKeys(browser.params.login.password);
               browser.findElement(by.className('button btn btn-success btn-large')).click();
               browser.sleep(4000);
   });
});