// Create First spec in Protractor

describe("Login Functionality", function ()
{
        it('Login Functionality with valid credentials', function () {
            browser.get("http://amasik.com/demo/angularjs/angular-app");
            browser.driver.manage().window().maximize();

            browser.driver.sleep(4000);

            // CSS locates elements using a CSS selector

            //Element Locator - Tag with id 'input#myid'
            // browser.findElement(by.css('input#Field')).click();

            //Element Locator - Tag with class 'input.field.login-checkbox'
            // browser.findElement(by.css('input.field.login-checkbox')).click();

            //Element Locator - Tag with any attribute
            // browser.findElement(by.css('input[tabindex="4"]')).click();

            //Element Locator - Tag with class any attribute input.field.login-checkbox[tabindex="4"]
            browser.findElement(by.css('input.field.login-checkbox[tabindex="4"]')).click();

            browser.driver.sleep(4000);
        });
    });