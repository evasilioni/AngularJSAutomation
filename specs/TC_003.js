describe("Login Functionality", function(){



    //before each and every test case
    beforeEach(function(){
    //the line below points that we are going to access properties that exist into config file
        browser.get(browser.params.url);
        console.log("*********************Test Case Started***********************");
    });


    afterEach(function(){
        console.log("*********************Test Case Ended***********************");
    });


    it("Login Data", function(){

        browser.driver.manage().window().maximize();
        browser.findElement(by.model('login.email')).clear();
        browser.findElement(by.model('login.email')).sendKeys("hello");
        browser.findElement(by.model('login.password')).sendKeys("hello");
    });

    it("Register User", function(){
        browser.findElement(by.partialLinkText('have an account?')).click();
        browser.sleep(4000);

        browser.findElement(by.name('firstname')).sendKeys('Hello');
        browser.findElement(by.name('lastname')).sendKeys('Testing');
        browser.findElement(by.name('email')).sendKeys(browser.params.email);
        browser.findElement(by.name('password')).sendKeys('123456');
        browser.findElement(by.name('confirm_password')).sendKeys('123456');
        browser.findElement(by.xpath('//label[text()="Agree with the Terms & Conditions."]')).click();
        browser.findElement(by.buttonText('Register')).click();
    });
})