// Create First spec in Protractor

describe("Login Functionality", function ()
{
        it('Login Functionality with valid credentials', function () {
            browser.get("http://amasik.com/demo/angularjs/angular-app");
            browser.driver.manage().window().maximize();

            browser.driver.sleep(4000);

            //Approach - 1 Tag with single attribute //input[@value='First Choice']
            // browser.findElement(by.xpath('//input[@value="First Choice"]')).click();

            //Approach - 2 Tag with multiple attribute with OR //input[@value='First Choice' or @tabindex="4"]

            //Approach - 3 Tag with multiple attribute with AND //input[@value='First Choice' and @tabindex="4"]

            //Approach - 4 Tag with multiple attribute with AND //input[@*='First Choice' and @tabindex="4"]
            // * stands for any
            // e.g //*[@*="First Choice" and @tabindex="4"]
            browser.findElement(by.xpath('//input[@*="First Choice" and @tabindex="4"]')).click();

            browser.driver.sleep(4000);
        });
    });

