// Create First spec in Protractor

describe("Login Functionality", function ()
    {
        it('Login Functionality with valid credentials', function () {
            browser.get("http://amasik.com/demo/angularjs/angular-app");
            browser.driver.manage().window().maximize();
            browser.driver.sleep(4000); // Check box will not be checked

            // Click on checkbox and using id element locator
            browser.findElement(by.id('Field')).click();
            browser.driver.sleep(5000);

            // Click on link - using partiallink text element locator
             browser.findElement(by.partialLinkText('have an account?')).click();

browser.driver.sleep(5000);
             //Click on back button
             browser.navigate().back();

                browser.driver.sleep(5000);
             //Click on Sign In btn using buttonText element locator
             browser.findElement(by.buttonText('Sign In')).click();
        });
    });