// Create First spec in Protractor

describe("Login Functionality", function ()
    {
        it('Login Functionality with valid credentials', function () {
            browser.get("http://amasik.com/demo/angularjs/angular-app");
            browser.driver.manage().window().maximize();

            //For Locating Element - Syntax 1
            // element(by.id('Field')).getWebElement().click();

            browser.driver.sleep(4000); // Check box will not be checked

            //For Locating Element - Syntax 2
            // browser.driver.findElement(by.id('Field')).click();
//
//            //For Locating Element - Syntax 3
//            browser.findElement(by.id('Field')).click();
//             browser.findElement(by.model('login.email')).sendKeys('Hello@abcd');

            browser.findElement(by.className("button btn btn-success btn-large")).click();
            // Element Locator : repeater
            browser.findElement(by.repeater("recentNews in data.response")).click();

            browser.findElement(by.buttonText("Sign In")).click();
            browser.findElement(by.partialButtonText("Sign")).click();

            // Check box will be checked
            browser.driver.sleep(10000);

            //fetch URL of page
            //expect(browser.getTitle()).toBe("AngularJS POC");
            //expect(browser.getCurrentUrl()).toBe("http://amasik.com/demo/angularjs/angular-app/#/login");
           // expect(browser.getPageSource()).toBe("http://amasik.com/demo/angularjs/angular-app/#/login");

        });
    });