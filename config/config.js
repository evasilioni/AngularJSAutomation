var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

exports.config={
    framework:'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs:[
//        '../specs/*.js'
        '../specs/FirstSpec.js',
//        '../specs/SecondSpec.js'
    ],
//    multiCapabilities:[{
//        'browserName':'chrome'
//    }
////     ,{
////         'browserName':'firefox'
////     }
//    ],
//    test:{
//        testdata : require('../samples/sample.json')
//    },
    params:
          {
              'common':
                   {
                          'url':"http://amasik.com/demo/angularjs/angular-app"
                   },
               'registration':
                   {
                        'firstName':"Testing",
                        'lastName':"World",
                        'email':"TestingWorldGreece@gmail.com",
                        'password':"testing"
                    },
               'login':
                  {
                      'username':"TestingWorldGreece@gmail.com",
                      'password':"testing"
                  }
         },

         onPrepare: function() {
            jasmine.getEnv().addReporter(
                new Jasmine2HtmlReporter({
                    savePath:'target/screenshots'
                })
            )
         }
}