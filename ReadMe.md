Setup Protractor 

1) Prerequisite: NodeJs, Jasmine
2) Install Protractor using npm: npm install -g protractor
# it will install two command line tools, protractor and webdriver-manager
3)Check protractor installed successfully or not
# protractor --version
4) Update Webdriver manager
#webdriver-manager update, helper tool to easily get instance of a selenium server running
5) Now start up a server with:
#webdriver-manager start

NOTE: This server should be started as pre-requisite to run protractor test cases


Protractor-Run test case on firefox browser :
capabilities:{
    'browserName':'firefox'
    }
    
Protractor-Run test case on multiple browsers (all test cases will run in parallel):
multiCapabilities:{
    'browserName':'firefox'
    }    
    

#Protractor - Supported Element Locators
model --> find an element by ng-model expression
repeater --> find  elements inside an ng-repeat
buttonText --> find a button by text
partialButtonText --> find a button by partial text    

# Protractor - Use beforeEach and afterEach
